
# Написать функцию isPrime, которая принимает число и возвращает простое число или нет.

def isPrime(a: int) -> bool:
    if a==2 or a==3: return True
    if a%2==0 or a<2: return False
    for i in range(3, int(a**0.5)+1, 2):
        if a%i==0:
            return False

    return True
a = int(input('Введите число: '))
print(isPrime(a))